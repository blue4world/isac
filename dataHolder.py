import fileinput
from data import *
import random
from frequencies import *

class DataHolder:
    def __init__(self):
        self.inputData = []
        self.outputData = []
        self.frequencies = WordFrequencies()

    def calculateWordsFrequencies(self):
        for data in self.inputData:
            line = data.getText()
            for word in line.split():
                self.frequencies.addWords(word)

    def loadToInputData(self):
        # load to input
        for line in fileinput.input():
            out = line.replace("\n", "").split("\t")
            data = Data(out[0], out[1])
            self.inputData.append(data)

    def loadFromFile(self):
        self.loadToInputData()
        self.calculateWordsFrequencies()

    def changeClass(self,fromClass,toClass):
        data = self._getDataWithClass(fromClass)
        for d in data:
            d.setClassName(toClass)

    def printData(self):
        for data in self.outputData:
            print data.getClassName()+"\t"+data.getText()

    def _getDataWithClass(self,className):
        dataWithClass = []
        for data in self.inputData:
            if data.getClassName() == className:
                dataWithClass.append(data)
        return dataWithClass

    def getDataWithClassWithNumOfSamples(self,className,numOfSamples):
        dataWithClass = self._getDataWithClass(className)
        dataWithClassFilter = []
        for i in range(0,numOfSamples):
            dataWithClassFilter.append(random.choice(dataWithClass))

        self.outputData = self.outputData + dataWithClassFilter


    def printWordFrequencies(self):
        self.frequencies.testPrint()

