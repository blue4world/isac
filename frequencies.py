import re

class WordFrequencies:
    def __init__(self):
        self.words = {}

    def sanitizeWord(self,word):
        return ''.join([i for i in word if i.isalpha()])


    def addWords(self,word):
        word = self.sanitizeWord(word)
        if word not in self.words:
            self.words[word] = 1
        else:
            self.words[word] = self.words[word] + 1


    def testPrint(self):
        d = self.words
        for w in sorted(d, key=d.get, reverse=True):
            print str(w)+";"+str(d[w])