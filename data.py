class Data:
    def __init__(self,className,text):
        self.className = className
        self.text = text

    def getText(self):
        return self.text

    def getClassName(self):
        return self.className

    def setClassName(self,className):
        self.className = className

